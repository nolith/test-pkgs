.PHONY: mvn npm

all: mvn npm

mvn:
	cd mvn && mvn deploy

npm:
	cd npm && npm publish 
